#Function Name: xorDecyptionHandler()
#Function Description: Takes a left and right operand and conducts an XOR operation
#on both.  This function assumes that left and right operands are hexadecimal
#and returns a decimal value.
def xorDecryptionHandler(leftOperand, rightOperand):
    #Variable declarations
    leftOperandInDecimal = 0
    rightOperandInDecimal = 0
    xoredResult = ""

    #Check if the left and right operands are non integers or are hexadecimal
    if not isinstance(leftOperand,int) and not isinstance(rightOperand,int):
        leftOperandInDecimal = int(leftOperand, 16)
        rightOperandInDecimal = int(rightOperand, 16)
        xoredResult = leftOperandInDecimal ^ rightOperandInDecimal
    elif isinstance(leftOperand,int) and isinstance(rightOperand,int):
        xoredResult = leftOperand ^ rightOperand
    return xoredResult