#Program: NanoBox.py
#Developer: Rich Hoggan
#Creation Date: 06/17/2016
#Description: NanoBox is a basic sandboxing application which can be used to decrypt XOR obfuscated values
#in X86 assembly.  NanoBox also attempts to detect register deletion by bypassing direct execution
#of detected code and executing in a separated environment which in turn preserves the decrypted value.

#Import directives
import sys

import FileHandler
import StackGenerator
import StackExecutionHandler
import XORDecryptionHandler
from Stack import Stack

#Function Code
#Function Name: printProgramHeader()
#Function Description: Prints the program's information header in the event that the command
#line argument was valid.
def printProgramHeader():
    print "NanoBox"
    print "Developed By: Rich Hoggan"

#Function Name: printUsageStatement()
#Function Description: Prints the program's usage statement when an error occurs
#from malformed command line arguments list.
def printUsageStatement():
    print "NanoBox"
    print "Developed By: Rich Hoggan"
    print ""
    print "Usage Statement:"
    print "NanoBox.py -decrypt <file name>"
    print "\tThis command will tell XOR to attempt decryption of the specimen's XOR'd content."
    print "NanoBox.py -decrypt -key <file name"
    print "\tThis command will tell XOR to attempt decryption fo the specimen's XOR'd"
    print "\tand will display the decryption key."

#Application Code
#Function Name: main()
#Function Description: The application's main method.
def main():
    #Variable declarations
    commandLineArguments = []
    commandLineArgumentsListLength = 0
    operationCommand = ""
    optionSwitch = ""
    specimenFileName = ""
    specimenFileContents = []
    generatedPushInstructionStack = Stack()
    specimenInstructionsList = []

    #Get the command line arguments and determine if the arguments are well formed
    commandLineArguments = sys.argv
    commandLineArgumentsListLength = len(commandLineArguments)

    """
    Acceptable commands include the following:
    NanoBox.py -decrypt <file name> -> 2 commands passed to program
    NanoBox.py -decrypt -key <file name> -> 3 commands passed to program
    """

    if commandLineArgumentsListLength == 3:
        printProgramHeader()

        #If the current length is 2 then the first command is expected
        #therefore get the operation mode and the file name from the command
        #line arguments and determine if they are valid
        operationMode = commandLineArguments[1]
        specimenFileName = commandLineArguments[2]
        if operationMode == "-decrypt" and specimenFileName != "":
            #Read the specimen file and return the file contents before generating the stack
            print ""
            print "Initial Setup"
            print "Reading speicmen file contents..."
            specimenFileContents = FileHandler.readSpecimenFile(specimenFileName)

            #Generate the push instruction and non-push instruction stack
            print "Generating stacks..."
            generatedPushInstructionStack = StackGenerator.generateStack(specimenFileContents,"PUSH_STACK")
            specimenInstructionsList = StackGenerator.generateStack(specimenFileContents,"INST_STACK")

            #Pass the generated stacks to the stack execution handler
            print "Running emulation handler..."
            print ""
            StackExecutionHandler.stackExecutionHandler(generatedPushInstructionStack,
                specimenInstructionsList)
        else:
            printUsageStatement()
    else:
        printUsageStatement()

main()