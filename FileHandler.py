#Function Name: readSpecimenFile()
#Function Description: Reads the specimen file and returns the file contents
#as a list.
def readSpecimenFile(specimenFileName):
    #Variable declarations
    specimenFileHandle = ""
    specimenFileContentsList = []

    #Attempt to load and read the specimen file
    try:
        specimenFileHandle = open(specimenFileName, "r")
        for currentFileLine in specimenFileHandle:
            specimenFileContentsList.append(currentFileLine)
    except IOError:
        print "Unable to continue."
        print "The file could not be opened or read."
        print "Perhaps its currupted?"

    #Return speicmenFileContentsList on completion
    return specimenFileContentsList