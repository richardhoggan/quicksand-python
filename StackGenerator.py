#Import directives
from Stack import Stack

#Function Name: generateStack()
#Function Description: Iterates through the specimen file contents and generates
#a stack based on the specified command flag.
#Available Flags:PUSH_STACK, INST_STACK
def generateStack(specimenFileContentsList, commandFlag):
    #Variable declarations
    specimenFileContentsListLength = 0
    generatedPushStack = Stack()
    generatedInstructionExecutionList = []

    #Validate the specimenFileContentsList argument has content before generating stack
    specimenFileContentsListLength = len(specimenFileContentsList)
    if specimenFileContentsListLength > 0:
        #Determine if the command flag is PUSH_STACK or INST_STACK
        if commandFlag == "PUSH_STACK":
            #Iterate through the specimen file and determine if there are push
            #instructions in order to generate the stack
            for currentFileLine in specimenFileContentsList:
                #Split and format the currentFileLine before looking for
                #push op codes
                currentFileLine = currentFileLine.strip()
                currentFileLineSplit = currentFileLine.split(" ")

                #Determine if the first index is a push opcode
                if currentFileLineSplit[0] == "push" or currentFileLineSplit[0] == "PUSH":
                    #Determine if the push instruction is attempting to push 0x0 or 0X0
                    #onto the generated stack
                    if currentFileLineSplit[1] != "0x0" and currentFileLineSplit[1] != "0X0":
                        generatedPushStack.push(currentFileLineSplit[1])
        elif commandFlag == "INST_STACK":
            #Iterate through the specimen file and determine if there are non PUSH
            #instructions in order to generate the stack
            for currentFileLine in specimenFileContentsList:
                #Split and format the currentFileLine before looking for the non-push op codes
                currentFileLine = currentFileLine.strip()
                currentFileLineSplit = currentFileLine.split(" ")

                #Determine if the first index is a non-push opcode
                if currentFileLineSplit[0] != "push" and currentFileLineSplit[0] != "PUSH":
                    generatedInstructionExecutionList.append(currentFileLine)

    #Return the generated stack on completion
    if commandFlag == "PUSH_STACK":
        return generatedPushStack
    elif commandFlag == "INST_STACK":
        return generatedInstructionExecutionList