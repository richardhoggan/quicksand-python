# README #

###Read Me Introduction
This read me discusses a brief overview of the NanoBox application, it's available features, development attributes, and how to work with the application.  

###Project Description
In certain instances, malicious attackers, malware authors, etc., will attempt to obfuscate encryption keys, or other values which might be used within a malicious application.  Based the likelihood of values being obfuscated, de-obfuscated and subsequently removed from memory after use, etc., NanoBox is an application which was developed as a research application for reading X86 assembly, conducting XOR operations on discovered data, and detecting whether or not the specimen is attempting to delete and/or overwrite known register locations.  

Upon discovery of malicious actions within the specimen, NanoBox immediately prompts the user to "spawn" off the execution flow of the specimen emulation in order to isolate potentially malicious actions while will allowing them to execute.  If emulation finishes successfully, the potential decrypted value will be displayed.

Finally, even though this application is considered a research application, with the express purpose of answering research and development questions, NanoBox will be considered for future development and feature generation.  

###NanoBox Features
NanoBox currently provides the following features: 

* Emulation of X86 obfuscation techniques within X86 assembly code specimens. 
* De-obfuscation of obfuscated values
* Malicious action detection

###Development Attributes
NanoBox was developed using Python 2.7, and was designed to be a cross-platform application able to run on the "big three" operating systems.  Additional support is also provided for the following mobile operating systems: 

* IOS - Using the Pythonista IOS app, it is possible to fun the full version of NanoBox minus the command line arguments.  
* Android - Using SL4A Remix (available at the link in the references section) it is possible to run the full version of the application minus the command line arguments. 
* Windows 10 Phones - (Currently under development).

** Please note that mobile versions of the application are being tested and will be added as soon as testing completes. 

###Running NanoBox
Finally, desktop versions of the application can be run on the command line using the NanoBox.py file within the associated application directory.  