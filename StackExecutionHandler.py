import XORDecryptionHandler
from collections import OrderedDict

#Function Name: stackExecutionHandler()
#Function Description: Executes the instructions in the specimen file.
def stackExecutionHandler(generatedPushInstructionsStack, specimenInstructionsList):
    #Variable declarations
    generatedPushInstructionStackLength = 0
    specimenInstructionsListLength = 0
    eaxRegister = ""
    mainMemory = OrderedDict()
    overwriteDetectionFlag = False
    isolatedMainMemory = OrderedDict()
    decodedString = ""

    #Get the length of the push instructions stack and the instructions
    #stack and validate there is data before continuing
    generatedPushInstructionStackLength = generatedPushInstructionsStack.size()
    specimenInstructionsListLength = len(specimenInstructionsList)
    if generatedPushInstructionStackLength > 0 and specimenInstructionsListLength:
        #Iterate through the specimen's instructions list and attempt to conduct XOR deobfuscation
        generatedPushInstructionsStack.reverseTheStack()
        for currentInstruction in specimenInstructionsList:
            #Format the current instruction and split into opcode and operands
            currentInstruction = currentInstruction.strip()
            currentInstructionSplit = currentInstruction.split(" ")
            currentInstructionOpCode = currentInstructionSplit[0]

            #Print the current instruction pointer
            print "Instruction Pointer->\t", currentInstruction

            #Determine which opcode is currently being executed
            """
            Available OpCodes include the following:
            pop - this will be a pop command off the stack
            xor - this will be a logical XOR operation on two operands
            mov - this will be a move instruction which will move a value to memory

            Available registers include the following:
            eax - this will be a typical representation of the EAX hardware register
            """
            if currentInstructionOpCode == "pop":
                """
                Pop Instruction Emulation
                Instruction->pop eax
                Instruction Description->This instruction will pop the next value from the
                stack and will store it in the eax register

                index 0     index 1
                pop         eax
                """

                #Get the operand and prepare to conduct the operation
                popInstructionOperand = currentInstructionSplit[1]

                #Determine the destination for the popped value
                if popInstructionOperand == "eax" or popInstructionOperand == "EAX":
                    #If the pop instruction operand is pointing to a registry location then pop from the
                    #stack and push to the register
                    currentStackLocation = generatedPushInstructionsStack.pop()
                    eaxRegister = currentStackLocation

                    #Print the execution status for the instruction
                    print "Instruction Execution->\tPopping the current value of the stack"
            elif currentInstructionOpCode == "xor":
                """
                xor Instruction Emulation
                Instruction->xor eax, 0x41
                Instruction Description->This instruction will conduct an XOR operation
                on the left and right operands.

                index 0      index 1     index 2
                xor         eax         0x41
                """

                #Get the operands and remove the comma
                leftOperand = currentInstructionSplit[1]
                rightOperand = currentInstructionSplit[2]
                leftOperand = leftOperand.replace(",","")

                #Determine if the left operand is pointing to a register location
                if leftOperand == "eax" and rightOperand != "eax":
                    #If the left operand is pointing to a register location, pass the
                    #register value as the left operand to the xor operation
                    xoredResult = XORDecryptionHandler.xorDecryptionHandler(eaxRegister, rightOperand)

                    #Upon completion move the xoredResult back into eax
                    eaxRegister = xoredResult

                    #Print th execution status for the instruction
                    print "Instruction Execution->\tConducting XOR operation"
                    print "Instruction Execution->\tEAX register now contains",eaxRegister
                elif leftOperand == "eax" and rightOperand == "eax":
                    #If both the left and right operands are pointing to the same register location
                    #pass the same register location to the XOR decryption handler
                    xoredResult = XORDecryptionHandler.xorDecryptionHandler(eaxRegister, eaxRegister)

                    #Upon completion move the xoredResult back into eax
                    eaxRegister = xoredResult

                    #Print th execution status for the instruction
                    print "Instruction Execution->\tConducting XOR operation"
                    print "Instruction Execution->\tEAX register now contains",eaxRegister
            elif currentInstructionOpCode == "mov":
                """
                mov Instruction Emulation
                Instruction->mov [0], eax
                Instruction Description: This instruction will move the value in the right operand
                to the memory location referenced in the left operand.

                index 0     index 1     index 2
                mov         [0]         eax
                """

                #Get the operands and remove the comma
                leftOperand = currentInstructionSplit[1]
                rightOperand = currentInstructionSplit[2]
                leftOperand = leftOperand.replace(",","")

                #Determine if the right operand is pointing to a registry location
                if rightOperand == "eax":
                    #If the right operand points to a registry location then get the value
                    #and move to the main memory at the specified location
                    #Determine if the the current memory address has a value in it
                    currentAddressValue = mainMemory.get(leftOperand)
                    if currentAddressValue == None and overwriteDetectionFlag != True:
                        mainMemory[leftOperand] = eaxRegister

                        #Print the execution status for the instruction
                        print "Instruction Execution->\tConducting MOV operation"
                        print "Instruction Execution->\tMemory address",leftOperand,"now contains",eaxRegister
                        # print mainMemory.get(leftOperand)
                        print ""
                    elif currentAddressValue != None and overwriteDetectionFlag != True:
                        #Print execution status message that a potentially dangerous operation was detected
                        print "Instruction Execution->\tA MOV operation was detected which appears to attempt to"
                        print "\t->overwrite an existing memory address."

                        #Prompt user for input - isolationResponse
                        print ""
                        isolationResponse = raw_input("Do you want to isolate remaining execution [y/n]?")
                        if isolationResponse != "y" and isolationResponse != "n":
                            while isolationResponse != "y" and isolationResponse != "n":
                                print "Unable to continue."
                                print "The response provided was empty."
                                print "Your response should be y or n."

                                #Prompt user for input - isolationResponse
                                isolationResponse = raw_input("Do you want to isolate remaining execution [y/n]?")

                        #Set the overwriteDetectionFlag according to user response and initialize the
                        #isolated memory data structure
                        if isolationResponse == "y":
                            overwriteDetectionFlag = True

                            print "Initializing isolation memory..."
                            print ""
                            isolatedMainMemory = mainMemory.copy()

                            # If the currentAddressValue is None and the user wants to isolate execution then
                            # mov the value into the isolatedMainMemory
                            isolatedMainMemory[leftOperand] = eaxRegister

                            #Print execution status message
                            print "Instruction Execution->\tConducting [ISOLATED] MOV operation"
                            print "Instruction Execution->\tIsolated Memory location",leftOperand,"contains",\
                                isolatedMainMemory.get(leftOperand)
                            print ""
                        else:
                            print "Instruction Execution->\tTerminating emulation..."
                            break
                    elif currentAddressValue != None and overwriteDetectionFlag:
                        #If the currentAddressValue is None and the user wants to isolate execution then
                        #mov the value into the isolatedMainMemory
                        isolatedMainMemory[leftOperand] = eaxRegister

                        #Print execution status message
                        print "Instruction Execution->\tConducting [ISOLATED] MOV operation"
                        print "Instruction Execution->\tIsolated Memory location", leftOperand, "contains", \
                            isolatedMainMemory.get(leftOperand)
                        print ""

        #Decode the decrypted values
        print ""
        for currentKey in mainMemory:
            currentAsciiValue = mainMemory.get(currentKey)
            currentCharacter = chr(currentAsciiValue)
            decodedString = decodedString + currentCharacter


        print "Decoded String:",decodedString